package mikheychev;

import mikheychev.exceptions.InterfaceException;
import mikheychev.exceptions.TaskManagerException;
import mikheychev.hierarchy.Department;
import mikheychev.hierarchy.Director;
import mikheychev.hierarchy.Workable;
import mikheychev.management.ConsoleInterface;
import mikheychev.management.Interface;
import mikheychev.management.TaskManager;
import mikheychev.tasks.Task;
import mikheychev.tasks.TaskFactory;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {

            Director director = Director.director();
            List<Department> deps = new ArrayList<>();

            Interface console = ConsoleInterface.instance();
            String csvPath = console.showLoadDlg();

            TaskManager tm = TaskManager.taskManager();
            if (!tm.readStructure(csvPath, director, deps)) {
                System.out.println("Wrong company configuration. Correct it and try again.");
                System.exit(0);
            }

            do {
                Interface.MainMenuButton btn = console.showMainMenu();
                switch (btn) {
                    case BROADCAST_TASK:
                        ConsoleInterface.TaskMenuButton task_btn = console.showAllTasks();
                        if(task_btn != ConsoleInterface.TaskMenuButton.EXIT &&
                                task_btn != ConsoleInterface.TaskMenuButton.UNKNOWN_TASK) {

                            Task task = TaskFactory.createTask(task_btn);
                            if(task != null)
                                tm.setBroadcast(deps, task);
                        }
                        break;
                    case DEPARTMENT_TASK:
                        Department dep = console.showDepartments(deps);
                        if(dep != null){

                            ConsoleInterface.TaskMenuButton task_btn2 = console.showAllTasks();
                            if(task_btn2 != ConsoleInterface.TaskMenuButton.EXIT &&
                               task_btn2 != ConsoleInterface.TaskMenuButton.UNKNOWN_TASK) {

                                Task task = TaskFactory.createTask(task_btn2);
                                if(task != null)
                                    tm.setDepartmentTask(dep, task);
                            }
                        }
                        break;
                    case EMPLOYEE_TASK:
                        Department dep2 = console.showDepartments(deps);
                        if(dep2 != null){

                            Workable employee = console.showEmployees(dep2);
                            if(employee != null) {
                                ConsoleInterface.TaskMenuButton task_btn3 = console.showTasks(employee);
                                if (task_btn3 != ConsoleInterface.TaskMenuButton.EXIT &&
                                    task_btn3 != ConsoleInterface.TaskMenuButton.UNKNOWN_TASK) {

                                    Task task = TaskFactory.createTask(task_btn3);
                                    if (task != null)
                                        tm.setEmployeeTask(employee, task);
                                }
                            }
                        }
                        break;
                    case PRINT_TASK:
                        console.showCompanyStructure(director, deps);
                        break;
                    case UNKNOWN_TASK:
                    case EXIT_TASK:
                        System.exit(0);
                }
            }
            while (true);
        } catch (InterfaceException | TaskManagerException e) {
            System.out.println("Program was finished with error: " + e.getMessage());
        }
    }
}
