package mikheychev.management;

import mikheychev.exceptions.InterfaceException;
import mikheychev.hierarchy.Department;
import mikheychev.hierarchy.Director;
import mikheychev.hierarchy.Workable;

import java.util.List;

public interface Interface {
    MainMenuButton showMainMenu();
    Department showDepartments(List<Department> deps) throws InterfaceException;
    TaskMenuButton showTasks(Workable employee) throws InterfaceException;
    TaskMenuButton showAllTasks() throws InterfaceException;
    Workable showEmployees(Department department) throws InterfaceException;
    void showCompanyStructure(Director director, List<Department> deps);
    String showLoadDlg();

    enum MainMenuButton { BROADCAST_TASK, DEPARTMENT_TASK, EMPLOYEE_TASK, PRINT_TASK, EXIT_TASK, UNKNOWN_TASK}
    enum TaskMenuButton { WRITE_PROGRAM, DESIGN_PROGRAM, TEST_PROGRAM, PLAN_TESTING,
        TRANSLATE_DOC, PAY_SALARY, FORM_REPORT, HAVE_VACATION, CLEAN_WORKSPACE, EXIT, UNKNOWN_TASK};
}
