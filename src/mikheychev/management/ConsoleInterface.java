package mikheychev.management;

import mikheychev.exceptions.InterfaceException;
import mikheychev.hierarchy.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ConsoleInterface implements Interface {

    public static ConsoleInterface instance(){
        if(instance == null)
            instance = new ConsoleInterface();

        return instance;
    }
    private static ConsoleInterface instance;
    private ConsoleInterface(){}

    @Override
    public MainMenuButton showMainMenu() {
        clearConsole();
        System.out.println("-----Company management program-----");
        System.out.println(" 1 - Set broadcast task");
        System.out.println(" 2 - Set department task");
        System.out.println(" 3 - Set employee task");
        System.out.println(" 4 - Print company structure");
        System.out.println(" 5 - Exit");

        try{
            String str = br.readLine();
            switch(str){
                case "1":
                    return MainMenuButton.BROADCAST_TASK;
                case "2":
                    return MainMenuButton.DEPARTMENT_TASK;
                case "3":
                    return MainMenuButton.EMPLOYEE_TASK;
                case "4":
                    return MainMenuButton.PRINT_TASK;
                case "5":
                    return MainMenuButton.EXIT_TASK;
                case "6":
                    return MainMenuButton.UNKNOWN_TASK;
            }
        }
        catch(IOException e) {
            System.out.println("Can't read line.");
            System.exit(0);
        }

        return MainMenuButton.UNKNOWN_TASK;
    }

    @Override
    public Department showDepartments(List<Department> deps) throws InterfaceException {

        clearConsole();

        if(deps == null)
            throw new InterfaceException("Department list is null");

        for(int i = 1; i < deps.size() + 1; i++){
            System.out.println(i + " - " + deps.get(i-1).getName());
        }
        System.out.println(deps.size() + 1 + " - Exit" );

        try {
            String str = br.readLine();
            Byte ind = Byte.parseByte(str);

            if(ind > deps.size()+1)
                throw new InterfaceException("There is no department with such number: " + ind);

            if(ind == deps.size()+1)
                return null;

            return deps.get(ind - 1);
        }
        catch(IOException | NumberFormatException e){
            throw new InterfaceException(e.getMessage());
        }
    }

    @Override
    public TaskMenuButton showTasks(Workable employee) throws InterfaceException {

        if(employee == null)
            throw new InterfaceException("Can't show tasks for employee which is null");

        clearConsole();

        if(employee instanceof Developer){
            System.out.println("1 - Write a program");
            System.out.println("2 - Design a program");
            System.out.println("3 - Have a vacation");
            System.out.println("4 - Clean a workspace");
            System.out.println("5 - Go back");
        } else if(employee instanceof Tester){
            System.out.println("1 - Test a program");
            System.out.println("2 - Plan testing");
            System.out.println("3 - Have a vacation");
            System.out.println("4 - Clean a workspace");
            System.out.println("5 - Go back");
        } else if(employee instanceof Writer){
            System.out.println("1 - Translate a document");
            System.out.println("2 - Have a vacation");
            System.out.println("3 - Clean a workspace");
            System.out.println("4 - Go back");
        } else if(employee instanceof Accountant){
            System.out.println("1 - Pay salary");
            System.out.println("2 - Form a report");
            System.out.println("3 - Have a vacation");
            System.out.println("4 - Clean a workspace");
            System.out.println("5 - Go back");
        } else {
            System.out.println("1 - Go back");
        }

        try{
            String str = br.readLine();
            Byte ind = Byte.parseByte(str);

            if(employee.getClass() == Developer.class){
                if(ind > 5)
                    throw new InterfaceException("Invalid task number");

                if(ind == 1)
                    return TaskMenuButton.WRITE_PROGRAM;
                else if(ind == 2)
                    return TaskMenuButton.DESIGN_PROGRAM;
                else if(ind == 3)
                    return TaskMenuButton.HAVE_VACATION;
                else if(ind == 4)
                    return TaskMenuButton.CLEAN_WORKSPACE;
                else if(ind == 5)
                    return TaskMenuButton.EXIT;

            } else if(employee.getClass() == Tester.class){
                if(ind > 5)
                    throw new InterfaceException("Invalid task number");

                if(ind == 1)
                    return TaskMenuButton.TEST_PROGRAM;
                else if(ind == 2)
                    return TaskMenuButton.PLAN_TESTING;
                else if(ind == 3)
                    return TaskMenuButton.HAVE_VACATION;
                else if(ind == 4)
                    return TaskMenuButton.CLEAN_WORKSPACE;
                else if(ind == 5)
                    return TaskMenuButton.EXIT;

            } if(employee.getClass() == Writer.class){
                if(ind > 4)
                    throw new InterfaceException("Invalid task number");

                if(ind == 1)
                    return TaskMenuButton.TRANSLATE_DOC;
                else if(ind == 2)
                    return TaskMenuButton.HAVE_VACATION;
                else if(ind == 3)
                    return TaskMenuButton.CLEAN_WORKSPACE;
                else if(ind == 4)
                    return TaskMenuButton.EXIT;

            } if(employee.getClass() == Accountant.class){
                if(ind > 5)
                    throw new InterfaceException("Invalid task number");

                if(ind == 1)
                    return TaskMenuButton.PAY_SALARY;
                else if(ind == 2)
                    return TaskMenuButton.FORM_REPORT;
                else if(ind == 3)
                    return TaskMenuButton.HAVE_VACATION;
                else if(ind == 4)
                    return TaskMenuButton.CLEAN_WORKSPACE;
                else if(ind == 5)
                    return TaskMenuButton.EXIT;

            } else return TaskMenuButton.UNKNOWN_TASK;
        }
        catch (IOException | NumberFormatException e) {
            throw new InterfaceException(e.getMessage());
        }

        return TaskMenuButton.UNKNOWN_TASK;
    }

    @Override
    public TaskMenuButton showAllTasks() throws InterfaceException {
        clearConsole();

        System.out.println("1 - Write a program");
        System.out.println("2 - Design a program");
        System.out.println("3 - Test a program");
        System.out.println("4 - Plan testing");
        System.out.println("5 - Translate a document");
        System.out.println("6 - Pay salary");
        System.out.println("7 - Form a report");
        System.out.println("8 - Have vacation");
        System.out.println("9 - Clean workspace");
        System.out.println("10 - Go back");

        try{
            String str = br.readLine();
            Byte b = Byte.parseByte(str);
            switch(b){
                case 1: return TaskMenuButton.WRITE_PROGRAM;
                case 2: return TaskMenuButton.DESIGN_PROGRAM;
                case 3: return TaskMenuButton.TEST_PROGRAM;
                case 4: return TaskMenuButton.PLAN_TESTING;
                case 5: return TaskMenuButton.TRANSLATE_DOC;
                case 6: return TaskMenuButton.PAY_SALARY;
                case 7: return TaskMenuButton.FORM_REPORT;
                case 8: return TaskMenuButton.HAVE_VACATION;
                case 9: return TaskMenuButton.CLEAN_WORKSPACE;
                case 10: return TaskMenuButton.EXIT;
            }
        } catch (IOException | NumberFormatException e) {
            throw new InterfaceException(e.getMessage());
        }

        return TaskMenuButton.UNKNOWN_TASK;
    }

    @Override
    public Workable showEmployees(Department dep) throws InterfaceException {

        if(dep == null)
            throw new InterfaceException("Department is null");

        clearConsole();

        List<Workable> employees = dep.getEmployees();
        if(employees == null)
            throw new InterfaceException("Employee list for the department is null");

        for(int i = 1; i<employees.size()+1; i++)
            System.out.println(i + " - " + employees.get(i-1).toString());
        System.out.println(employees.size() + 1 + " - Go back");

        try {
            String str = br.readLine();
            Byte ind = Byte.parseByte(str);

            if(ind > employees.size()+1)
                throw new InterfaceException("There is no employee with such number: " + ind);

            if(ind == employees.size()+1)
                return null;

            return employees.get(ind - 1);
        }
        catch(IOException | NumberFormatException e){
            throw new InterfaceException(e.getMessage());
        }
    }

    @Override
    public void showCompanyStructure(Director director, List<Department> deps) {
        System.out.println("Director: " + director.name());
        System.out.println("----------------------------");
        for(Department dep: deps){
            System.out.println("Department: " + dep.getName());
            System.out.println("\tChief: " + dep.getChief().name());
            List<Workable> employees = dep.getEmployees();
            for(Workable employee: employees){
                System.out.println("\t" + employee.getClass().getSimpleName() + ": " + ((Employee)employee).name());
            }
            System.out.println("----------------------------");
        }
    }

    private static final String DEFAULT_CSV_PATH = "./test_data";

    @Override
    public String showLoadDlg() {

        String path = null;
        try {
            String cmd = null;
            do {
                clearConsole();
                System.out.println("Do you want to load default configuration ("+DEFAULT_CSV_PATH+") (Y, n)");
                cmd = br.readLine();
            }
            while(!cmd.equals("Y") && !cmd.equals("y") && !cmd.equals("N") && !cmd.equals("n"));

            if (cmd.equals("Y") || cmd.equals("y")) {
                path = DEFAULT_CSV_PATH;
            } else if (cmd.equals("N") || cmd.equals("n")) {
                clearConsole();
                System.out.println("Input configuration directory path: . . .");
                path = br.readLine();
            }

        }
        catch(IOException e) {
            System.out.println("Can't read line.");
            System.exit(0);
        }

        return path;
    }

    private final static void clearConsole()
    {
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                Runtime.getRuntime().exec("cls");
            }
            else
            {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final IOException e)
        {
            System.out.println("Can't clear the console: " + e.getMessage());
        }
    }

    private static BufferedReader br;
    static {
        try{
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        catch(NullPointerException e){
            System.out.println("Console isn't defined. Program is n't able to work.");
            System.exit(0);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        br.close();
    }
}
