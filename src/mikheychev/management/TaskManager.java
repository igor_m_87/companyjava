package mikheychev.management;

import mikheychev.exceptions.EmployeeFactoryException;
import mikheychev.exceptions.TaskManagerException;
import mikheychev.hierarchy.*;
import mikheychev.tasks.Task;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;

public class TaskManager {

    public static TaskManager taskManager(){
        if(instance == null)
            instance = new TaskManager();

        return instance;
    }
    public void setBroadcast(List<Department> deps, Task task) throws TaskManagerException {

        if(deps == null)
            throw new TaskManagerException("Invalid departments");

        if(task == null)
            throw new TaskManagerException("Invalid task");

        for(Department dep: deps){
            List<Workable> employees = dep.getEmployees();
            if(employees == null) continue;

            for(Workable employee: employees)
                employee.doTask(task);
        }
    }
    public void setDepartmentTask(Department dep, Task task) throws TaskManagerException {
        if(dep == null)
            throw new TaskManagerException("Invalid department");

        if(task == null)
            throw new TaskManagerException("Invalid task");

        List<Workable> employees = dep.getEmployees();
        for(Workable employee: employees)
            employee.doTask(task);
    }
    public void setEmployeeTask(Workable employee, Task task) throws TaskManagerException {
        if(employee == null)
            throw new TaskManagerException("Invalid employee");

        if(task == null)
            throw new TaskManagerException("Invalid task");

        employee.doTask(task);
    }
    public boolean readStructure(String path, Director director, List<Department> deps) {
        File csvDir= new File(path);
        if(!csvDir.exists() || !csvDir.isDirectory()){
            System.out.println("'" + path + "' isn't a valid directory");
            return false;
        }

        ExecutorService executor = Executors.newCachedThreadPool();

        File[] files = csvDir.listFiles();
        ArrayList<Future<Boolean>> futures = new ArrayList<>(files.length);
        for(File file: files) {

            String ext = fileExtention(file.getName());
            if(!ext.equals(EXT)) continue;

            Future<Boolean> future = executor.submit(new Callable<Boolean>() {
                public Boolean call() throws Exception{

                    String pureName = filePureName(file.getName());
                    Department dep = null;
                    if(!file.getName().equals(DIRECTOR_CSV)) {
                        dep = new Department(pureName);
                        deps.add(dep);
                    }

                    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

                        String str = null;
                        while((str = reader.readLine()) != null){

                            if(str.isEmpty() || str.equals(CSV_HEADER)) continue;

                            int ind = str.indexOf(CSV_DELIMITER);
                            if(ind <= 0) continue;

                            String name_str = str.substring(0,ind);
                            String position_str = str.substring(ind + 1);

                            Workable.Position position = Workable.Position.valueOf(position_str.toUpperCase());

                            if(position == Workable.Position.DIRECTOR)
                                Director.director().setName(name_str);
                            else if (position == Workable.Position.CHIEF){
                                Chief chief = new Chief(name_str);
                                dep.setChief(chief);
                            }
                            else{
                                Workable employee = EmployeeFactory.createEmployee(name_str, position);
                                dep.getEmployees().add(employee);
                            }
                        }

                    } catch (FileNotFoundException e) {
                        System.out.println(e.getMessage());
                        return false;
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                        return false;
                    } catch (EmployeeFactoryException e) {
                        System.out.println(e.getMessage());
                        return false;
                    }

                    return true;
                }

            });
        }

        try{
            Iterator<Future<Boolean>> fit = futures.iterator();
            while (fit.hasNext()){
                Future<Boolean> future = fit.next();
                while(!future.isDone()){
                    Thread.sleep(THREAD_SLEEP);
                }
            }

            executor.shutdown();
            executor.awaitTermination(FILE_THREADS_TIMEOUT, TimeUnit.SECONDS);
        }
        catch(InterruptedException e){
            if(!executor.isTerminated())
                executor.shutdownNow();
        }

        return true;
    }
    private static final String EXT = "csv";
    private static final String CSV_HEADER = "Name;Position";
    private static final String CSV_DELIMITER = ";";
    private static final String DIRECTOR_CSV = "Director.csv";
    private static final int FILE_THREADS_TIMEOUT = 5;
    private static final int THREAD_SLEEP = 100;
    private String fileExtention(String name){
        if(name == null)
            return "";

        int ind = name.lastIndexOf(".");
        if(ind <= 0)
            return "";

        return name.substring(ind + 1);

    }
    private String filePureName(String name){
        if(name == null)
            return "";

        int ind = name.lastIndexOf(".");
        if(ind <= 0)
            return "";

        return name.substring(0,ind);
    }

    private static TaskManager instance;
}
