package mikheychev.exceptions;

public class EmployeeFactoryException extends Exception {
    public EmployeeFactoryException(String msg) {
        super(msg);
    }
}
