package mikheychev.exceptions;

public class InterfaceException extends Exception {
    public InterfaceException(String msg){
        super(msg);
    }
}
