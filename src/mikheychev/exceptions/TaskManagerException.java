package mikheychev.exceptions;

public class TaskManagerException extends Exception {
    public TaskManagerException(String msg){
        super(msg);
    }
}
