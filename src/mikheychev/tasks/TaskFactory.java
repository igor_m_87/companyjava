package mikheychev.tasks;

import mikheychev.management.Interface;

public class TaskFactory {
    private TaskFactory(){}
    public static Task createTask(Interface.TaskMenuButton btn){
        Task task = null;
        switch (btn){
            case PAY_SALARY:
                task = new PaySalaryTask();
                break;
            case FORM_REPORT:
                task = new FormReportTask();
                break;
            case PLAN_TESTING:
                task = new PlanTestTask();
                break;
            case TEST_PROGRAM:
                task = new TestProgramTask();
                break;
            case HAVE_VACATION:
                task = new HaveVacation();
                break;
            case TRANSLATE_DOC:
                task = new TranslateDocTask();
                break;
            case WRITE_PROGRAM:
                task = new WriteProgramTask();
                break;
            case CLEAN_WORKSPACE:
                task = new CleanWorkspaceTask();
                break;
            case DESIGN_PROGRAM:
                task = new DesignProgramTask();
                break;
        }

        return task;
    }
}
