package mikheychev.hierarchy;

import mikheychev.tasks.Task;

public interface Workable {
    void doTask(Task task);
    enum Position {DIRECTOR, CHIEF, DEVELOPER, TESTER, WRITER, ACCOUNTANT, UNKNOWN}
}
