package mikheychev.hierarchy;

import mikheychev.tasks.CommonTask;
import mikheychev.tasks.Task;
import mikheychev.tasks.WriterTask;

public class Writer extends Employee implements Workable {

    public Writer(String name) {
        super(name);
    }

    @Override
    public void doTask(Task task) {
        if(task instanceof WriterTask ||
                task instanceof CommonTask)
        {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task has been done");
        } else {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task hasn't been done");
        }
    }

    @Override
    public String toString() {
        return this.name();
    }
}
