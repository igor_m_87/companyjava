package mikheychev.hierarchy;

public class Chief extends Employee{

    public Chief(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return this.name();
    }
}
