package mikheychev.hierarchy;

import java.util.ArrayList;
import java.util.List;

public class Department {

    public Department(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private Employee chief;

    private List<Workable> employees = new ArrayList<>();

    public Employee getChief() {
        return chief;
    }

    public void setChief(Employee chief) {
        this.chief = chief;
    }

    public List<Workable> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Workable> employees) {
        this.employees = employees;
    }
}
