package mikheychev.hierarchy;

import mikheychev.tasks.CommonTask;
import mikheychev.tasks.DeveloperTask;
import mikheychev.tasks.Task;

public class Developer extends Employee implements Workable {

    public Developer(String name) {
        super(name);
    }

    @Override
    public void doTask(Task task) {
        if(task instanceof DeveloperTask ||
                task instanceof CommonTask)
        {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task has been done");
        } else {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task hasn't been done");
        }
    }

    @Override
    public String toString() {
        return this.name();
    }
}
