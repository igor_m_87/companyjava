package mikheychev.hierarchy;

public class Director extends Employee{

    private static Director director;
    public static Director director(){
        if(director == null)
            director = new Director();

        return director;
    }

    private Director(){
        super();
    }

    @Override
    public String toString() {
        return this.name();
    }
}
