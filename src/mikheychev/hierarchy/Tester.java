package mikheychev.hierarchy;

import mikheychev.tasks.CommonTask;
import mikheychev.tasks.Task;
import mikheychev.tasks.TesterTask;

public class Tester extends Employee implements Workable {
    public Tester(String name) {
        super(name);
    }

    @Override
    public void doTask(Task task) {
        if(task instanceof TesterTask ||
                task instanceof CommonTask)
        {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task has been done");
        } else {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task hasn't been done");
        }
    }

    @Override
    public String toString() {
        return this.name();
    }
}
