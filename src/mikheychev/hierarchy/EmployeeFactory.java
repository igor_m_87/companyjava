package mikheychev.hierarchy;

import mikheychev.exceptions.EmployeeFactoryException;

import static mikheychev.hierarchy.Workable.*;

public class EmployeeFactory {
    private EmployeeFactory(){}
    public static Workable createEmployee(String name, Position position)
            throws EmployeeFactoryException {

        switch(position){
            case DEVELOPER:
                return new Developer(name);
            case TESTER:
                return new Tester(name);
            case WRITER:
                return new Writer(name);
            case ACCOUNTANT:
                return new Accountant(name);
            case UNKNOWN:
                throw new EmployeeFactoryException("Unknown employee position.");
        }

        return null;
    }
}
