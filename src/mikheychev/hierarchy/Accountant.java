package mikheychev.hierarchy;

import mikheychev.tasks.AccountantTask;
import mikheychev.tasks.CommonTask;
import mikheychev.tasks.Task;

public class Accountant extends Employee implements Workable{

    public Accountant(String name) {
        super(name);
    }

    @Override
    public void doTask(Task task) {
        if(task instanceof AccountantTask ||
                task instanceof CommonTask)
        {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task has been done");
        } else {
            System.out.println(name() + "(" + this.getClass().getSimpleName() + "):" + "Task hasn't been done");
        }
    }

    @Override
    public String toString() {
        return this.name();
    }
}
